package com.codosoft.topis2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Topis2AdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(Topis2AdminApplication.class, args);
	}

}
