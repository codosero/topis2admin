package com.codosoft.topis2.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class AdminRequest {
	
	private String username;
	private String language;

}